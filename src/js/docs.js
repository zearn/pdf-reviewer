/* 
 * Collection of documents
 * Since the UI allows several open documents, this class helps to manages
 * state changes in the UI depending on how many documents are open.
 */

import Log from "./log.js";
import Doc from "./doc.js";
import Corpora from "./corpora.js";

export default class Docs {

  constructor(container, adobeClientId, log = new Log(), langs = ["en", "es"], version = 1) {
    this.log = log;
    this.docs = {};
    this.context = this.#addContext(container);
    this.adobeClientId = adobeClientId;
    this.status = true;
    this.loadCorpora(langs, version);
  }

  loadCorpora(langs, version) {
    this.#toggleStatus();
    new Corpora(langs, version, this.log, (_) => {
      this.#toggleStatus();
    });
  }

  add(file) {
    const name = file.name.trim().replace(".pdf", ""),
      id = "pdf-" + name.replace(/\W+/g, "-");
    if (!this.get(name)) {
      this.docs[id] = new Doc({
        id: id,
        name: name,
        file: file,
        ...this.context,
      }, this.adobeClientId, this.log);
    } else {
      this.docs[id].focus();
    }
    return this.docs[id];
  }

  focus(query) {
    this.#try(query, "focus");
  }

  remove(query) {
    this.#try(query, "remove");
  }

  toggle(query) {
    this.#try(query, "toggle");
  }

  activate(query) {
    this.#try(query, "activate");
  }

  deactivate(query) {
    this.#try(query, "deactivate");
  }

  get(query) {
    for (const [id, doc] of Object.entries(this.docs)) {
      if (id == query || doc.id == query || doc.name == query) {
        return doc;
      }
    }
    return null;
  }

  refresh() {
    for (const [id, doc] of Object.entries(this.docs)) {
      if (doc.dom.container.parentNode == null) {
        delete this.docs[id];
        continue;
      }
      if (doc.isFocus()) {
        this.activeDoc =  doc;
      }
    }
  }

  #try(query, fnName) {
    const doc = this.get(query);
    if (doc) {
      doc[fnName]();
    }
  }

  #addContext(container) {
    const html = `
      <div id="docs-tabs" class="tabs is-boxed is-small"><ul></ul></div>
      <div id="docs-containers"></div>`;
    container.innerHTML = html;
    this.refresh();
    new MutationObserver(() => {
      this.refresh();
    }).observe(container, {
      childList: true,
      subtree: true,
      attributes : true
    });
    return {
      tabs: container.querySelector("#docs-tabs ul"),
      containers: container.querySelector("#docs-containers"),
    };
  }

  #toggleStatus() {
    this.status = !this.status;
    if (this.status) {
      this.dom.parentNode.parentNode.remove();
      delete this.dom;
    } else {
      const html = `
      <div class="blocker has-text-centered">
        <div>
          <p>Loading Data...</p>
          <progress class="progress is-primary" max="100"></progress>
        </div>
      </div>`;
      this.context.containers.insertAdjacentHTML("beforeend", html);
      this.dom = this.context.containers.querySelector(".progress");
    }
  }
}

