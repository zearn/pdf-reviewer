/*
 * Owner's information
 */

const logo = {
  src: "https://cdn.prod.website-files.com/60ad603a6b6b23851c3fb0d8/60b785b4f4632f386760ded7_Zearn-Logo.svg",
  alt: "Zearn is the top-rated math learning platform that helps kids explore concepts, discover meaning, and make sense of math",
  href: "https://about.zearn.org/",
}

// NOTE: key == JSON file ./src/rules/key.json
// NOTE: Don't use "custom" as key
export const rules = {
  "tea-es": "Texas Spanish (TEA's Choice)",
  "tx-es": "Texas Spanish",
  "us-es": "National Spanish",
  "typos": "Potential Typos"
}

export default logo;
