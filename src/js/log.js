/* 
 * User actitivy logger 
 * It helps for debugging purposes when unexpected behaviors happen.
 */

export default class Log {

  constructor(debug = true) {
    this.debug = debug;
    this.data = [];
  }

  puts() {
    const msg = [...arguments].join(" ");
    if (this.debug) {
      console.debug("APP:", msg);
    }
    this.add(msg);
  }

  add(msg) {
    this.data.push(`${Date.now()}: ${msg}`);
  }
}
