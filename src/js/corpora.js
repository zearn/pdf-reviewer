/*
 * Corpora management
 * The "corpora" are the datasets by language of all the  annotated words in a 
 * collection of documents; i.e. all Zearn documents. 
 * NOTE: The corpora are made outside this repo.
 */

import Log from "./log.js";

export default class Corpora {

  // Variable name when it gets exposed in globalThis
  globalKey = "corpora";

  constructor(langs, version, log = new Log(), callback = null) {
    this.log = log;
    this.langs = langs;
    this.version = version;
    this._callback = callback;
    this.info = {};
    this.load();
  }

  load() {
    for (let i = 0; i < this.langs.length; i++) {
      const lang = this.langs[i],
        path = `corpora/${lang}.csv`,
        msg = `Loading '${path}'`;
      this.log.puts(msg);
      fetch(path)
        .then((res) => res.text())
        .then((text) => {
          this.#parse(text, lang);
          if (i + 1 == this.langs.length) {
            globalThis[this.globalKey] = this;
            if (this._callback) {
              this._callback(this);
              delete this._callback;
            }
          }
        })
        .catch((er) => this.log.puts(`${msg} failed: ${er.message}`));
    }
  }

  // NOTE: Text.search depends on this
  search(query, key = "word", count = -1, lang = "en", list) {
    list = list || this.info[lang];
    const findings = [];
    for (const token of list) {
      if (count >= 0 && findings.length >= count){
        break;
      }
      if (this.#match(query, token, key)) {
        findings.push(token);
      }
    }
    return findings;
  }

  #parse(text, lang) {
    this.info[lang] = [];
    for (const row of text.trim().split("\n").slice(1)) {
      const cols = row.split("\t");
      this.info[lang].push({
        count: parseInt(cols[1]),
        word: cols[0],
        lemma: cols[2],
        pos: cols[3],
        morph: cols[4],
      });
    }
  }

  #match(query, token, key) {
    try {
      const rgx = new RegExp(query),
        val = token[key];
      var groups, sym, num;
      switch (key) {
        // NOTE: When the string value is actually a number
        case "page":
        case "line":
        case "count":
        case "length":
          groups = /(?<sym>(<|<=|>|>=|!=){0,1})(?<num>\d+)/.exec(query).groups;
          sym = groups.sym ? groups.sym : "==";
          num = parseInt(groups.num);
          return Function(`return ${val} ${sym} ${num}`)();
        // NOTE: When the string value remains as string
        default:
          // NOTE: Some values (like lemma and morph) have several values divided by a pipe
          for (const str of val.split("|")) {
            if (rgx.test(str)) {
              return true;
            }
          }
      }
    } catch (_) {
      return false;
    }
  }
}
