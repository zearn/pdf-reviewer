import logo from "./info.js";
import Log from "./log.js";
import UI from "./ui.js";
import Docs from "./docs.js";
import RFBox from "./rfbox.js";

/* General */

const layout = {
  logo: logo,
  menu: [{
    name: "File",
    items: [{
      name: "Open...",
      withPicker: true,
      fnName: "mOpen"
    }, {
      name: "Log",
      fnName: "mLog"
    }, {
      type: "divider"
    }, {
      name: "Close",
      fileRequired: true,
      fnName: "mClose"
    }],
  }, {
    name: "Edit",
    items: [{
      name: "Find...",
      fileRequired: true,
      fnName: "mFind"
    }, {
      name: "Review...",
      fileRequired: true,
      fnName: "mReview"
    }],
  }, ]
};

var log = new Log();

/* Listeners */

window.addEventListener("error", er => {
  const fileName = er.filename.split("/").reverse()[0],
    msg = `${er.type}:${fileName}:${er.lineno}:${er.colno}: ${er.message}`;
  log.add(msg);
  return false;
});

window.addEventListener("load", () => {
  globalThis.ui = new UI(document.querySelector("section"), layout, log);
  globalThis.ui.ready(() => {
    globalThis.docs = new Docs(document.getElementById("docs-wrapper"), globalThis.adobeAPIKey, log);
    globalThis.rfbox = new RFBox(log, globalThis.ui);
  });
});

/* Menu Functions */

window.mOpen = (ev) => {
  for (const file of ev.target.files) {
    if (globalThis.docs.status) {
      globalThis.docs.add(file).focus();
    } else {
      log.puts("Rejecting doc opening: status not ready");
    }
  }
};

window.mClose = () => {
  for (const [id, doc] of Object.entries(globalThis.docs.docs)) {
    if (doc.dom.tab.classList.contains("is-active")) {
      globalThis.docs.remove(id);
      break;
    }
  }
};

window.mLog = () => {
  const link = document.createElement("a"),
    blob = new Blob([log.data.join("\n")], {
      type: 'plain/text'
    });
  link.href = URL.createObjectURL(blob);
  link.download = "zearn-pdf-app-log.txt";
  link.click();
  link.remove();
};

window.mReview = () => {
  const boxId = "box-review";
  if (globalThis.ui.getBox(boxId)) {
    globalThis.ui.focusBox(boxId);
  } else {
    globalThis.ui.addBox(boxId, globalThis.rfbox.layout());
  }
};

window.mFind = () => {
  const boxId = "box-find";
  if (globalThis.ui.getBox(boxId)) {
    globalThis.ui.focusBox(boxId);
  } else {
    globalThis.ui.addBox(boxId, globalThis.rfbox.layout("find"));
  }
};
