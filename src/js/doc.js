/*
 * Document management
 * A document implies the required APIs and the text management capabilities.
 */

import Log from "./log.js";
import Text from "./text.js";

export default class Doc {

  constructor(context, adobeClientId, log = new Log()) {
    this.log = log;
    this.log.puts(`Initializing doc '${context.id}'`);
    this.id = context.id;
    this.name = context.name;
    this.file = context.file;
    this.lang = this.name.includes("_ES") ? "es" : "en";
    this.text = new Text(this.lang);
    this.status = false;
    this.numPages = 0;
    this.metadata = "";
    this.context = {
      tabs: context.tabs,
      containers: context.containers,
    };
    this.dom = {
      container: this.#addContainer(),
      tab: this.#addTab(),
      tree: document.createElement("root"),
    };
    this.apis = {};
    this.#adobeRead(adobeClientId, () => {
      this.#pdfjsRead();
    });
  }

  isFocus() {
    return this.dom.tab.classList.contains("is-active");
  }

  focus() {
    this.log.puts(`Focusing doc '${this.id}'`);
    Array.from(this.dom.tab.parentNode.children).forEach(tab => {
      tab.classList.remove("is-active");
    });
    Array.from(this.dom.container.parentNode.children).forEach(container => {
      container.classList.add("no-display");
    });
    this.dom.tab.classList.add("is-active");
    this.dom.container.classList.remove("no-display");
  }

  remove() {
    this.log.puts(`Removing doc '${this.id}'`);
    if (this.dom.tab.nextElementSibling) {
      this.dom.tab.nextElementSibling.querySelector("span").click();
    } else if (this.dom.tab.previousElementSibling) {
      this.dom.tab.previousElementSibling.querySelector("span").click();
    }
    Object.values(this.dom).forEach(el => el.remove());
  }

  toggle() {
    if (this.status) {
      this.activate();
    } else {
      this.deactivate();
    }
  }

  activate() {
    this.status = true;
    if (this.dom.progress) {
      this.log.puts(`Enabling '${this.id}' for user actions`);
      this.dom.progress.parentNode.parentNode.remove();
      delete this.dom.progress;
    }
  }

  deactivate(msg = "Loading", callback = null) {
    this.status = false;
    if (!this.dom.progress) {
      this.log.puts(`Disabling '${this.id}' for user actions: ${msg}`);
      const html = `
      <div class="blocker has-text-centered">
        <div>
          <p>${msg}...</p>
          <progress class="progress is-primary" max="100"></progress>
        </div>
      </div>`;
      this.dom.container.insertAdjacentHTML("beforeend", html);
      this.dom.progress = this.dom.container.querySelector(".progress");
    }
    if (callback) {
      callback();
    }
  }

  updateProgress(msg, curr = 0, max = 0) {
    this.dom.progress.parentNode.querySelector("p").innerHTML = `${msg}...`;
    if (max != 0) {
      this.dom.progress.value = curr;
      this.dom.progress.max = max;
    }
  }

  gotoLocation(page, line = 0) {
    const [x, y] = line == 0 ? [0, 0] : this.#getLine(page, line);
    this.focus();
    this.apis.adobe.apis.gotoLocation(page, x, y);
  }

  #getLine(page, line) {
    const size = this.text.pages[page].size;
    const ln = this.text.pages[page].lines[line];
    const x = ln.transform[0] + ln.transform[4];
    const y = size[1] - ln.transform[3] - ln.transform[5];
    return [parseInt(x), parseInt(y)];
  }

  #addContainer() {
    const html = `<div id="${this.id}" class="doc file"><div id="${this.id}-adobe"></div></div>`;
    this.context.containers.insertAdjacentHTML("beforeend", html);
    return this.context.containers.lastChild;
  }

  #addTab() {
    const html = `
      <li><a>
        <span>${this.name}</span><button class="delete is-small"></button>
      </a></li>`;
    this.context.tabs.insertAdjacentHTML("beforeend", html);
    for (const tag of ["span", "button"]) {
      const child = this.context.tabs.lastChild.querySelector(tag);
      if (tag == "span") {
        child.addEventListener("click", () => this.focus());
      } else {
        child.addEventListener("click", () => this.remove());
      }
    }
    return this.context.tabs.lastChild;
  }

  /* Adobe API */

  #adobeConfig = {
    pdfEmbed: {
      defaultViewMode: "FIT_WIDTH",
      /*enableAnnotationAPIs: true,
        includePDFAnnotations: true,*/
      enableSearchAPIs: true,
      showDisabledSaveButton: true
    }
  };

  #adobeRead(clientId, callback) {
    this.deactivate("Loading document");
    const msg = `Initializing '${this.name}' with Adobe API`;
    const reader = new FileReader();
    reader.readAsArrayBuffer(this.file);
    reader.onloadend = ev => {
      this.log.puts(msg);
      const adobeDCView = new AdobeDC.View({
        clientId: clientId,
        divId: `${this.id}-adobe`
      });
      adobeDCView.previewFile({
        content: {
          promise: Promise.resolve(ev.target.result)
        },
        metaData: {
          fileName: this.name
        }
      }, this.#adobeConfig.pdfEmbed).then(pdf => {
        this.apis.adobe = pdf;
        pdf.getAPIs().then(apis => {
          this.apis.adobe.apis = apis;
          this.activate();
          callback();
        });
      });
    };
    reader.onerror = er => {
      this.log.puts(`${msg} failed: ${er.message}`);
      this.remove();
    };
  }

  /* PDFjs API */

  #pdfjsRead() {
    this.deactivate("Analyzing document");
    const msg = `Initializing '${this.name}' with PDFjs API`;
    const reader = new FileReader();
    reader.readAsDataURL(this.file);
    reader.onloadend = ev => {
      this.log.puts(msg);
      const fileData = atob(ev.target.result.replace(/^.+?,/, ""));
      globalThis.pdfjsLib.getDocument({
        data: fileData
      }).promise.then(pdf => {
        this.apis.pdfjs = pdf;
        this.numPages = pdf.numPages;
        pdf.getMetadata().then(metadata => this.metadata = metadata);
        this.#pdfjsExtractPage();
      });
    };
    reader.onerror = er => {
      this.log.puts(`${msg} failed: ${er.message}`);
      this.remove();
    };
  }

  #pdfjsExtractPage(numPage = 1) {
    this.updateProgress(`Analyzing page ${numPage} of ${this.numPages}`, numPage, this.numPages);
    this.apis.pdfjs.getPage(numPage).then(page => {
      const pageView = new globalThis.pdfjsViewer.PDFPageView({
        container: document.createElement("page"),
        id: numPage,
        scale: 1,
        defaultViewport: page.getViewport(),
        eventBus: new globalThis.pdfjsViewer.EventBus(),
      });
      this.text.addPage(page.getViewport().viewBox.slice(-2));
      this.text.addLine();
      pageView.setPdfPage(page);
      pageView.draw().then(() => this.dom.tree.appendChild(pageView.div));
      page.getTextContent().then(content => {
        content.items.forEach(item => {
          if (item.hasEOL) {
            this.text.addLine();
          } else if (item.str && item.str.trim()) {
            this.text.concatLine(item);
          }
        });
      }).then(() => {
        this.text.clear();
        if (numPage < this.numPages) {
          this.#pdfjsExtractPage(numPage + 1);
        } else {
          this.text.sort().analyze();
          this.activate();
        }
      });
    });
  }
}
