/*
 *  Text management and analysis
 *  All the cool linguistic stuff for each document goes here.
 */

export default class Text {

  constructor(lang, extraPatterns = []) {
    this.lang = lang;
    this.patterns = extraPatterns.concat([
      // TODO: Patterns for floats, currency and units; concats several token in one
    ]);
    // NOTE: Text as a string with all content
    this.text = "";
    // NOTE: Text as an array of pages; page as array of lines; line as obj with text, transform & size
    this.pages = {};
    // NOTE: Text as an array of tokens; each token as {label, lemma, morph, pos, word, print, page, line}
    this.tokens = [];
    // NOTE: Text as an array of unique sorted tokens; each ref as {label, lemma, morph, pos, word, count}
    this.refs = [];
  }

  addPage(size, numPage) {
    // NOTE: size == [width, height] 
    size = size || Array(2).fill(0);
    numPage = numPage || Object.keys(this.pages).length + 1;
    this.pages[numPage] = {
      size: size,
      lines: {},
    };
    return this.pages[numPage];
  }

  addLine(numPage) {
    numPage = numPage || Object.keys(this.pages).length;
    const numLine = Object.keys(this.pages[numPage].lines).length + 1;
    this.pages[numPage].lines[numLine] = {
      text: "",
      // NOTE: line.transform type == CSS matrix(); cfr. https://www.w3schools.com/css/css3_2dtransforms.asp
      transform: Array(6).fill(0),
      // NOTE: size == [width, height] 
      size: Array(2).fill(0),
    };
    return this.pages[numPage].lines[numLine];
  }

  concatLine(item, numPage, numLine) {
    numPage = numPage || Object.keys(this.pages).length;
    numLine = numLine || Object.keys(this.pages[numPage].lines).length;
    const line = this.pages[numPage].lines[numLine];
    let string = item.str;
    if (this._previousItem && line.text) {
      const [vec1, vec2] = [this._previousItem.transform, item.transform];
      if (Math.round(vec1[4] + this._previousItem.width) != Math.round(vec2[4])) {
        string = " " + string;
      }
    } else {
      // NOTE: line.transform == first item to be concatenated in the line; it ignores further item transforms
      line.transform = item.transform;
    }
    line.text += string;
    line.size[0] += item.width;
    line.size[1] = line.size[1] > item.height ? line.size[1] : item.height;
    this._previousItem = item;
    return line.text;
  }

  clear() {
    Object.keys(this).forEach(key => {
      if (key[0] == "_") {
        delete this[key];
      } else if (key == "tokens") {
        this[key] = [];
      } else if (key == "text") {
        this[key] == "";
      }
    });
  }

  sort() {
    Object.values(this.pages).forEach((page) => {
      let lines = Object.entries(page.lines);
      lines = lines.map(arr => arr[1]);
      lines = lines.sort((a, b) => b.transform[5] - a.transform[5]);
      lines = lines.map((obj, index) => ([index + 1, obj]));
      page.lines = Object.fromEntries(lines);
    });
    return this;
  }

  normalize(string) {
    return string.normalize("NFKD").replace(/\p{Diacritic}/gu, "").toLowerCase();
  }

  analyze() {
    this.clear();
    this._refs = {};
    Object.entries(this.pages).forEach(([numPage, page]) => {
      Object.entries(page.lines).forEach(([numLine, line]) => {
        const text = line.text + "\n",
          tokens = this.tokenize(text, true).map(t => {
            return {
              ...t,
              page: parseInt(numPage),
              line: parseInt(numLine),
            };
          });
        this.text += text;
        this.tokens = this.tokens.concat(tokens);
      });
    });
    this.refs = Object.values(this._refs).sort((a, b) => a.word > b.word);
    delete this._refs;
    return this;
  }

  tokenize(string, count = false, _pos = [0, 0]) {
    const chars = string.split("");
    let tokens = [],
      print = {};
    for (let i = 0; i < chars.length; i++) {
      const character = chars[i],
        label = this.#identifyChar(character);
      if (Object.keys(print).length === 0) {
        print[label] = character;
      } else if (label in print && ["alpha", "digit", "space"].includes(label)) {
        print[label] += character;
      } else {
        tokens = this.#addToken(tokens, print, count);
        print = {};
        print[label] = character;
      }
    }
    return this.#addToken(tokens, print, count);
  }

  search(rawQuery, listName = "tokens") {
    const list = this[listName];
    var req = this.#verifySearch(rawQuery, list);
    if (req.status) {
      return this.#doSearch(req, list);
    } else {
      return req;
    }
  }

  #verifySearch(rawQuery, list) {
    var req = {
      status: list == this.tokens || list == this.refs,
      query: [],
      res: []
    };
    if (req.status) {
      try {
        req.query = typeof rawQuery === "string" ? JSON.parse(rawQuery) : rawQuery;
        // NOTE: Valid type structure == Array(Array(String, String)...)
        var t1 = req.query.flat(1).map(e => Array.isArray(e));
        var t2 = req.query.flat(2).map(e => typeof e === "string");
        var typesOk = !t1.concat(t2).includes(false);
        req.status = typesOk && t1.length * 2 == t2.length;
      } catch (_) {
        req.status = false;
      }
    }
    return req;
  }

  // TODO: Refactor, mainly in if (term) block
  #doSearch(req, list, context = 5) {
    var res = [];
    var query = [...req.query];
    for (var i = 0; i < list.length; i++) {
      var token = list[i];
      var term = query.shift();
      if (term) {
        var [match, finding] = this.#matchSearch(term, token);
        if (match) {
          res.push(finding[0]);
        } else {
          query = [...req.query];
          res = [];
          term = query.shift();
          [match, finding] = this.#matchSearch(term, token);
          if (match) {
            res.push(finding[0]);
          } else {
            query = [...req.query];
            res = [];
          }
        }
      }
      if (res.length == req.query.length) {
        var iPre = i - res.length + 1;
        req.res.push({
          before: list.slice(iPre - context < 0 ? 0 : iPre - context, iPre),
          match: res,
          after: list.slice(i + 1, i + context + 1)
        });
        query = [...req.query];
        res = [];
      }
    }
    return req;
  }

  #matchSearch(query, token) {
    var finding;
    for (var atom of query) {
      const args = [atom[1], atom[0], 1, this.lang, [token]];
      finding = globalThis.corpora.search(...args);
      if (finding.length == 0) {
        return [false, []];
      }
    }
    return [true, finding];
  }

  #identifyChar(character) {
    if (/\p{L}/u.test(character)) {
      return "alpha";
    } else if (/\p{N}/u.test(character)) {
      return "digit";
    } else if (/\p{P}/u.test(character)) {
      return "punct";
    } else if (/\p{S}/u.test(character)) {
      return "symbl";
    } else if (/\s/.test(character)) {
      return "space";
    } else {
      return "other";
    }
  }

  #identifyCase(str) {
    const cases = {
      "lower": str.toLowerCase(),
      "upper": str.toUpperCase(),
      "caps": str.charAt(0).toUpperCase() + str.slice(1)
    };
    for (let [key, val] of Object.entries(cases)) {
      if (val == str) {
        return key;
      }
    }
    return "other";
  }

  #addToken(tokens, print, count) {
    const label = Object.keys(print)[0];
    if (label == "other") {
      return tokens;
    } else {
      const printText = Object.values(print)[0],
        word = printText.toLowerCase(),
        ref = this._refs[word] || false,
        defaults = label == "digit" ? ["NUM", "NumForm=Digit"] : ["X", "X"],
        // NOTE: Any new token attribute must be consider in Corpora.#match() switch block if the value is a number 
        token = {
          label: label,
          print: printText,
          word: word,
          lemma: ref && ref.lemma ? ref.lemma : "X",
          pos: ref && ref.pos ? ref.pos : defaults[0],
          morph: ref && ref.morph ? ref.morph : defaults[1],
          length: word.length,
          case: this.#identifyCase(printText)
        };
      tokens.push(this.#addRef(token, count));
      return tokens;
    }
  }

  #addRef(token, count) {
    if (["alpha", "digit", "punct", "symbl"].includes(token.label)) {
      if (!this._refs[token.word]) {
        const args = [`^${token.word}$`, "word", 1, this.lang];
        Object.assign(token, globalThis.corpora.search(...args)[0] || token);
        this._refs[token.word] = structuredClone(token);
        this._refs[token.word].count = 0;
        delete this._refs[token.word].print;
        delete token.count;
      }
      this._refs[token.word].count += count ? 1 : 0;
    }
    return token;
  }
}
