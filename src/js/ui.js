/* 
 * User interface
 * It displays and manages all the UI objects used by the user.
 */

import Log from "./log.js";

export default class UI {

  constructor(container, layout, log = new Log()) {
    this.log = log;
    this.status = false;
    this.container = container;
    this.#addUI(layout);
    this.items = Array.from(container.querySelectorAll(".menu-item"));
    this.boxes = Array.from(container.querySelectorAll(".box"));
    this.burger = this.container.querySelector(".navbar-burger");
    this.#activateUI();
  }

  ready(callback) {
    if (this.status) {
      callback();
    } else {
      this._callback = callback;
    }
  }

  refresh() {
    const ready = document.querySelector(".progress") ? false : true;
    const docsHTML = this.container.querySelectorAll("#docs-wrapper .doc");
    this.boxes = Array.from(this.container.querySelectorAll(".box"));
    this.items.forEach(item => {
      if ("fileRequired" in item.dataset) {
        if ((docsHTML && docsHTML.length == 0) || !ready) {
          item.classList.add("is-disabled");
        } else {
          item.classList.remove("is-disabled");
        }
      }
    });
    if (docsHTML && docsHTML.length == 0) {
      this.boxes.forEach(box => {
        if ("fileRequired" in box.dataset) {
          this.removeBox(box.id);
        }
      });
    }
  }

  addBox(boxId, layout) {
    this.log.puts(`Adding box '${boxId}'`);
    this.#insertBox(boxId, layout);
  }

  replaceBox(boxId, layout) {
    this.log.puts(`Replacing box '${boxId}'`);
    this.#insertBox(boxId, layout);
  }

  getBox(boxId, quiet = false) {
    const box = this.container.querySelector(`#${boxId}`);
    if (!box && !quiet) {
      this.log.puts(`Box '${boxId}' not found`);
    }
    return box;
  }

  focusBox(boxId) {
    this.log.puts(`Focusing box '${boxId}'`);
    const box = this.getBox(boxId);
    if (box) {
      box.scrollIntoView();
      if (!box.classList.contains("blink")) {
        box.classList.toggle("blink");
        setTimeout(() => {
          box.classList.toggle("blink");
        }, 1000);
      }
    }
  }

  removeBox(boxId) {
    const box = this.getBox(boxId);
    if (box) {
      this.log.puts(`Removing box '${boxId}'`);
      box.remove();
    }
  }

  #addUI(layout) {
    this.log.puts(`Initializing UI in '${this.container.outerHTML}'`);
    const html = `
      <nav id="ui" class="navbar" role="navigation" aria-label="main navigation">
        <div class="navbar-brand">
          <a class="navbar-item" href="${layout.logo.href}" target="_blank">
            <img src="${layout.logo.src}" alt="${layout.logo.alt}" width="112" height="28">
          </a>
          <a role="button" class="navbar-burger" aria-label="menu" aria-expanded="false" data-target="menu">
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
          </a>
        </div>
        <div id="menu" class="navbar-menu">
          <div class="navbar-start"></div>
        </div>
      </nav>
      <div id="content">
        <aside id="boxes"></aside>
        <article id="docs-wrapper"></article>
      </div>`;
    this.container.innerHTML = html;
    this.#addMenu(this.container.querySelector("#menu div"), layout.menu);
  }

  #addMenu(container, layout) {
    for (const mainItem of layout) {
      const html = `
        <div class="navbar-item has-dropdown is-hoverable">
          <a class="navbar-link">${mainItem.name}</a>
          <div class="navbar-dropdown"></div>
        </div>`;
      container.insertAdjacentHTML("beforeend", html);
      this.#addItems(container.lastChild.querySelector("div"), mainItem.items);
    }
  }

  #addItems(container, items) {
    for (const item of items) {
      const picker = '<input class="no-display picker" type="file" accept="application/pdf" multiple></input>',
        html = item.type == "divider" ? `<hr class="navbar-divider">` : `
          <a ${item.id ? `id="${item.id}"` : ""} class="navbar-item menu-item" ${item.fileRequired ? "data-file-required" : ""}>
            ${item.name}${item.include ? item.include : item.withPicker ? picker : ""}
          </a>`;
      container.insertAdjacentHTML("beforeend", html);
      if (!item.type || item.type != "divider") {
        this.#addFunction(item.fnName, container.lastElementChild);
      }
    }
  }

  #addFunction(fnName, item) {
    const fnOk = typeof window[fnName] === "function",
      picker = item.querySelector(".picker");
    if (fnOk) {
      if (picker) {
        item.addEventListener("click", () => picker.click());
        picker.addEventListener("change", ev => window[fnName](ev), false);
      } else {
        item.addEventListener("click", ev => {
          if (ev.target == item) {
            ev.preventDefault();
            window[fnName]();
          }
        });
      }
    } else {
      this.log.puts(`Function '${fnName}' not found`);
    }
  }

  #activateUI() {
    this.#activateBurger();
    this.#activateObserver();
    this.refresh();
    this.status = true;
    if (this._callback) {
      this._callback();
      delete this._callback;
    }
  }

  #activateBurger() {
    this.burger.addEventListener("click", ev => {
      const burger = ev.currentTarget,
        menu = document.getElementById(burger.dataset.target),
        msg = burger.classList.contains("is-active") ? "Closing" : "Opening";
      this.log.puts(`${msg} burger menu`);
      burger.classList.toggle("is-active");
      menu.classList.toggle("is-active");
    });
  }

  #activateObserver() {
    new MutationObserver(() => {
      this.refresh();
    }).observe(this.container, {
      childList: true,
      subtree: true
    });
  }

  #insertBox(boxId, layout) {
    const html = `
        <div id="${boxId}" class="box" ${layout.fileRequired ? "data-file-required" : ""}>
          <h2 class="subtitle">${layout.name}</h2>
          <button class="delete is-small"></button>
          <div class="box-content">
            ${layout.html}
          </div>
        </div>`;
    let box = this.getBox(boxId, true);
    if (box) {
      box.insertAdjacentHTML("beforebegin", html);
      box.remove();
    } else {
      this.container.querySelector("#boxes").insertAdjacentHTML("beforeend", html);
    }
    box = this.getBox(boxId);
    box.querySelector(".delete").addEventListener("click", () => this.removeBox(boxId));
    if (layout.event) {
      layout.event(box);
    }
  }
}
