/*
 * Review and find boxes management
 * The "Find" and "Review" menu buttons are very similar in the sense that
 * each "rule" for "Review" is a "query" for "Find"; the diff is that a "rule"
 * also includes a descriptive message for each Review query.
 */

import {
  rules as infoRules
} from "./info.js";

export default class RFBox {

  constructor(log, ui) {
    this.log = log;
    this.ui = ui;
  }

  layout(el = "review") {
    if (el == "review") {
      return {
        name: "Review",
        fileRequired: true,
        html: `
      <form>
        <div id="rules-select" class="field">
          <label class="label">Rules</label>
          <div class="control">
            <div class="select">
              <select name="rule">
                ${this.#addInfoRules()}
                <option value="custom">Custom</option>
              </select>
            </div>
            <input class="no-display picker" type="file" accept="application/json"></input>
          </div>
        </div>
        <div class="field is-grouped">
          <div class="control">
            <button class="button is-link">Submit</button>
          </div>
        </div>
      </form>`,
        event: (box) => {
          box.querySelector("form").addEventListener("submit", ev => {
            const data = this.#getData(ev.target);
            ev.preventDefault();
            this.log.puts(`Parsing rules '${data.rule}'`);
            if (data.rule != "custom") {
              this.#fetch(box, data.rule);
            } else {
              this.#read(box);
            }
          });
        },
      };
    } else {
      return {
        name: "Find",
        fileRequired: true,
        html: `
        <form>
          <div class="field">
            <label class="label">Queries</label>
            <div class="queries"></div>
          </div>
          <div class="field">
            <label class="label">Scope</label>
            <div class="control">
              <div class="select">
                <select name="scope">
                  <option value="cur">Current Document</option>
                  <option value="all">All Documents</option>
                </select>
              </div>
            </div>
          </div>
          <div class="field is-grouped">
            <div class="control">
              <button class="button is-link">Submit</button>
            </div>
          </div>
        </form>`,
        event: (box) => {
          this.#addQuery(box);
          box.querySelector("form").addEventListener("submit", ev => {
            const data = this.#getData(ev.target);
            ev.preventDefault();
            this.#addResults(box, data, "find");
          });
        }
      };
    }
  }

  toggleGroup(group) {
    document.querySelectorAll(`[data-group="${group}"]`).forEach(tr => {
      tr.classList.toggle('no-display');
    }); 
  }

  async #fetch(box, ruleName) {
    const req = new Request(`rules/${ruleName}.json?t=${new Date().valueOf()}`);
    try {
      const res = await fetch(req);
      const json = await res.json();
      this.#success(box, json);
    } catch (er) {
      this.#fail(box, req.url, er.message);
    }
  }

  #read(box) {
    const picker = box.querySelector(".picker");
    picker.click();
    picker.addEventListener("change", ev => {
      for (const file of ev.target.files) {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onloadend = ev => {
          const json = JSON.parse(atob(ev.target.result.replace(/^.+?,/, "")));
          this.#success(box, json);
        };
        reader.onerror = er => {
          this.#fail(box, file.name, er.message);
        };
      }
    }, false);
  }

  #fail(box, uri, error) {
    this.log.puts(`Reading rules '${uri}' failed: ${error}`);
    this.ui.replaceBox(box.id, {
      name: "Review",
      fileRequired: true,
      html: `
      <form>
        <div class="field">
          <p>Error while parsing rules.</>
        </div>
        <div class="field is-grouped">
          <div class="control">
            <button class="button is-link">New</button>
          </div>
        </div>
      </form>`,
      event: (box) => {
        box.querySelector("form").addEventListener("submit", ev => {
          ev.preventDefault();
          this.ui.replaceBox(box.id, this.layout());
        });
      }
    });
  }

  #success(box, json) {
    this.ui.replaceBox(box.id, {
      name: "Review",
      fileRequired: true,
      html: `
      <form>
        <div class="field">
          <label class="label">Queries</label>
          <details>
            <summary>View</summary>
            <div class="queries"></div>
          </details>
        </div>
        <div class="field">
          <label class="label">Scope</label>
          <div class="control">
            <div class="select">
              <select name="scope">
                <option value="cur">Current Document</option>
                <option value="all">All Documents</option>
              </select>
            </div>
          </div>
        </div>
        <div class="field is-grouped">
          <div class="control">
            <button class="button is-link">Cancel</button>
            <button class="button is-link">Submit</button>
          </div>
        </div>
      </form>`,
      event: (box) => {
        json.forEach(rule => this.#addQuery(box, rule));
        box.querySelector("form").addEventListener("submit", ev => {
          const data = this.#getData(ev.target, json);
          ev.preventDefault();
          if (ev.submitter.outerText == "Cancel") {
            this.ui.replaceBox(box.id, this.layout());
          } else {
            this.#addResults(box, data);
          }
        });
      }
    });
  }

  #addQuery(box, data = {}) {
    this.log.puts(`Adding query input in '${box.id}'`);
    const container = box.querySelector(".queries"),
      isEmpty = Object.keys(data).length == 0,
      queries = container.querySelectorAll("div.query"),
      helper = isEmpty ? '' : `<span class="tooltip-text">${data.msg}</span>`,
      closeBtn = `<div class="control"><a class="button is-danger">−</a></div>`,
      openBtn = `<div class="control"><a class="button is-success">+</a></div>`,
      // NOTE: Keep single quotes for the following value attr
      html = `<div class="field has-addons query tooltip">
        ${(queries.length > 0 && isEmpty) || !isEmpty ? closeBtn : ''}
        <div class="control input-div">
          <input class="input query" type="text" 
            name="query${queries.length + 1}" 
            placeholder="Query ${queries.length + 1}" 
            value='${!isEmpty ? JSON.stringify(data.query) : ''}'
            ${queries.length == 0 ? 'required' : ''}></input>
        </div>
        ${queries.length == 0 && isEmpty ? openBtn : ''}
        ${helper}
      </div>`;
    container.insertAdjacentHTML("beforeend", html);
    for (const btnSelector of ["a.is-danger", "a.is-success"]) {
      const btn = container.lastChild.querySelector(btnSelector);
      if (btn) {
        btn.addEventListener("click", (ev) => {
          if (btnSelector == "a.is-danger") {
            this.log.puts(`Removing query input in '${box.id}'`);
            let index = 1;
            ev.target.parentNode.parentNode.remove();
            box.querySelectorAll(`input.query`).forEach(query => {
              query.name = `query${index}`;
              query.placeholder = `Query ${index}`;
              index++;
            });
          } else {
            this.#addQuery(box);
          }
        });
      }
    }
  }

  #getData(form, rules = []) {
    var data = Object.fromEntries(new FormData(form));
    data.queries = [];
    data.docs = [];
    if (data.scope) {
      if (data.scope == "all") {
        data.docs = Object.values(globalThis.docs.docs).filter(d => d.status);
      } else if (data.scope == "cur") {
        data.docs = [globalThis.docs.activeDoc];
      }
      delete data.scope;
    }
    for (let i = 0; i < Object.keys(data).length; i++) {
      const [key, val] = Object.entries(data)[i];
      if (key.includes("query")) {
        data.queries.push([val, rules[i] ? rules[i].msg : ""]);
      }
    }
    return data;
  }

  #htmlResults(key, doc) {
    switch (key) {
      case "header":
        return `
          <details class="doc-details">
            <summary>
              <a onclick="docs.focus('${doc.id}')">
                <span class="doc-name">${doc.name}</span> 
                (<span class="doc-results">0 matches</span>)
              </a>
            </summary>
            <table class="results table is-striped is-narrow is-hoverable is-fullwidth is-size-7">
              <thead>
                <tr>
                  <th class="has-text-centered is-narrow"><abbr title="Match index">Num</abbr></th>
                  <th class="has-text-centered is-narrow"><abbr title="Page:Line">Page</abbr></th>
                  <th class="has-text-left">Result</th>
                </tr>
              </thead>
              <tbody>
        `
      case "failed":
        return `
          <tr>
            <th class="has-text-centered has-background-info has-text-primary-light" colspan="3">
              ${doc.query}
            </th>
          </tr>
          <tr class="status-failed">
            <td class="has-text-left" colspan="3">
              <p><b>Invalid JSON syntax.</b></p>
              <p>Did you <a href='https://jsonlint.com/?json=${doc.query}' target="_blank">validate it</a> before?</p>
              <p>A well-formed query has the following structure:<code>Query[Token[Attr[Key, RegExp]]]</code>.</p>
              <p>Available attribute keys: ${Object.keys(doc.text.tokens[0]).join(", ")}.</p>
              <p>Example for matching "Zearn": <code>[[["word","zearn"]]]</code></p>
              <p>Example for matching any digit + "Zearn": <code>[[["label","digit"]],[["word","zearn"]]]</code></p>
              <p>Example for matching any digit with 1 character + "Zearn": <code>[[["label","digit"], ["length", "1"]],[["word","zearn"]]]</code></p>
            </td>
          </tr>
        `
      case "subheader":
        return `
          <tr onclick="rfbox.toggleGroup('${doc.group}')" data-group="header-${doc.group}" class="pointer">
            <th class="has-text-centered has-background-info has-text-primary-light" colspan="3">
              ${doc.msg}
            </th>
          </tr>
        `
      case "match":
        return `
          <tr ondblclick="this.classList.toggle('strikethrough')" data-group="${doc.group}">
            <td class="index has-text-centered is-narrow">${doc.num}</td>
            <td class="has-text-centered is-narrow">
              <a onclick="docs.docs['${doc.id}'].gotoLocation(${doc.page}, ${doc.line})">
                ${doc.page}:${doc.line}
              </a>
            </td>
            <td class="has-text-left">${doc.text}</td>
          </tr>
        `
      case "footer":
        return `
              </tbody>
            </table>
          </details>
        `
    }
  }

  #getResultsText(finding) {
    return [
      finding.before.map(e => e.label == "space" ? " " : e.print).join(""),
      "<b>",
      finding.match.map(e => e.label == "space" ? " " : e.print).join(""),
      "</b>",
      finding.after.map(e => e.label == "space" ? " " : e.print).join("")
    ];
  }

  #getResults(data) {
    var idx = 0; 
    var num = 1;
    var csv = ["doc\tpage\tline\tbefore\tmatch\tafter\tmsg\tquery"];
    var json = [];
    var html = [];
    for (const doc of data.docs) {
      // TODO: doc.deactivate() not working as expected
      doc.deactivate("Searching");
      html.push(this.#htmlResults("header", doc));
      for (var [query, msg] of data.queries) {
        const findings = doc.text.search(query);
        msg = msg.replaceAll(/<\s*\w+[^>]+?>/g, " ");
        if (!findings.status) {
          html.push(this.#htmlResults("failed", {
            ...doc,
            query: query
          }));
        }
        if (msg.length != 0 && findings.res.length != 0) {
          idx += 1;
          html.push(this.#htmlResults("subheader", {
            ...doc,
            msg: msg,
            group: `group-${idx}`
          }));
        }
        for (var finding of findings.res) {
          const text = this.#getResultsText(finding);
          const page = finding.match[0].page;
          const line = finding.match[0].line;
          json.push({
            doc: doc.name,
            query: query,
            msg: msg,
            res: text.join(""),
            tokens: finding
          })
          csv.push(`${doc.name}\t${page}\t${line}\t${text[0]}\t${text[2]}\t${text[4]}\t${msg}\t${query}`);
          html.push(this.#htmlResults("match", {
            ...doc,
            num: num,
            page: page,
            line: line,
            text: text.join(""),
            group: `group-${idx}`
          }))
          num += 1;
        }
      }
      html.push(this.#htmlResults("footer", doc));
      doc.activate();
    }
    return {
      html: html.join(""),
      json: JSON.stringify(json, null, 2),
      csv: csv.join("\n")
    };
  }

  #addResults(box, data, el = "review") {
    const name = el == "review" ? "Review" : "Find";
    const results = this.#getResults(data);
    this.ui.replaceBox(box.id, {
      name: name,
      fileRequired: true,
      html: `
      <form>
        <div class="field">
          <label class="label">Results (<span class="all-results">0 matches</span>)</label>
          ${results.html}
        </div>
        <div class="field is-grouped">
          <div class="control">
            <button class="button is-link">New</button>
            <button class="button is-link">CSV</button>
            <button class="button is-link">JSON</button>
          </div>
        </div>
      </form>`,
      event: (box) => {
        box.querySelectorAll("summary").forEach(e => e.click());
        box.querySelectorAll("details table").forEach(table => {
          var resNum = [...table.querySelectorAll("td.index")].length;
          var allRes = table.parentNode.parentNode.querySelector(".all-results");
          var docRes = table.parentNode.querySelector(".doc-results");
          var strRes = resNum == 1 ? "match" : "matches";
          docRes.innerHTML = `${resNum} ${strRes}`;
          allRes.innerHTML = `${parseInt(allRes.innerHTML) + resNum} ${strRes}`;
          if (resNum == 0 && table.querySelector(".status-failed")) {
            table.querySelector("thead").remove();
          } else if (resNum == 0) {
            table.remove();
          }
        });
        box.querySelector("form").addEventListener("submit", ev => {
          ev.preventDefault();
          if (ev.submitter.outerText == "New") {
            this.ui.replaceBox(box.id, this.layout(el));
          } else if (ev.submitter.outerText == "CSV") {
            this.#openFile(results.csv);
          } else if (ev.submitter.outerText == "JSON") {
            this.#openFile(results.json, "json");
          }
        });
      }
    });
  }

  #addInfoRules() {
    return Object.entries(infoRules).map(([k, v]) => {
      return `<option value="${k}">${v}</option>`
    }).join("");
  }

  #openFile(str, type = "csv") {
    const file = {
      mimetype: type == "csv" ? "text/csv" : "application/json",
      ext: type == "csv" ? ".csv" : ".json"
    }
    const link = document.createElement("a"),
      blob = new Blob([str], {
        type: file.mimetype
      });
    link.href = URL.createObjectURL(blob);
    link.download = `zearn-pdf-app-results${file.ext}`;
    link.click();
    link.remove();
  }
}
