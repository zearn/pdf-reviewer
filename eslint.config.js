import globals from "globals";
import pluginJs from "@eslint/js";


export default [{
    languageOptions: {
      globals: {
        AdobeDC: "readonly",
        ...globals.browser
      }
    },
    rules: {
      "no-unused-vars": ["error", {
        "varsIgnorePattern": "^_",
        "argsIgnorePattern": "^_",
        "caughtErrors": "none",
      }]
    },
  },
  pluginJs.configs.recommended,
];
